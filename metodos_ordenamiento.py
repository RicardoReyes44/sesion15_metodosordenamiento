'''
Created on 19 nov. 2020

@author: RSSpe
'''

import random, time
from array import array


class MetodosOrdenamiento:

    def burbuja1(self, arreglo):
        inicio = time.time()
        for i in range(1, len(arreglo)):
            for j in range(0, len(arreglo)-i):
                if arreglo[j]>arreglo[j+1]:
                    aux = arreglo[j]
                    arreglo[j] = arreglo[j+1]
                    arreglo[j+1] = aux
        fin = time.time()
        print(f"Tiempo: {fin-inicio}")


    def burbuja2(self, arreglo):
        i = 0
        inicio = time.time()
        while i<len(arreglo):
            j=i
            while j<len(arreglo):
                if arreglo[i]>arreglo[j]:
                    aux = arreglo[i]
                    arreglo[i] = arreglo[j]
                    arreglo[j] = aux
                j+=1
            i+=1
        fin = time.time()
        print(f"Tiempo: {fin-inicio}")


    def burbuja3(self, arreglo):
        cont = 1
        inicio = time.time()
        while cont<len(arreglo):
            for j in range(0, len(arreglo)-cont):
                if arreglo[j]>arreglo[j+1]:
                    aux = arreglo[j]
                    arreglo[j] = arreglo[j+1]
                    arreglo[j+1] = aux
            cont+=1
        fin = time.time()
        print(f"Tiempo: {fin-inicio}")
    
    def insercion(self, numeros):
        aux=0
        inicio = time.time()
        for i in range(1, len(numeros)):
            aux = numeros[i]

            j=(i-1)
            while (j>=0 and numeros[j]>aux):
                numeros[j+1] = numeros[j]
                numeros[j] = aux
                j-=1
        fin = time.time()
        print(f"Tiempo: {fin-inicio}")
    
    def shellSort(self, numeros):
        intervalo = int(len(numeros)/2)
        inicio = time.time()
        while(intervalo>0):
            for i in range(int(intervalo), len(numeros)):
                j=i-int(intervalo)
                while(j>=0):
                    k=j+int(intervalo)
                    if numeros[j]<=numeros[k]:
                        j-=1
                    else:
                        aux=numeros[j]
                        numeros[j]=numeros[k]
                        numeros[k]=aux
                        j-=int(intervalo)
            intervalo=int(intervalo)/2
        fin = time.time()
        print(f"Tiempo: {fin-inicio}")
    
    def ordenamientoSeleccion(self, numeros):
        inicio = time.time()
        for i in range(len(numeros)):
            for j in range(i, len(numeros)):
                if(numeros[i] > numeros[j]):
                    orden = numeros[i]
                    numeros[i] = numeros[j]
                    numeros[j] = orden
        fin = time.time()
        print(f"Tiempo: {fin-inicio}")
    
    def ejecutar(self, numeros):
        inicio = time.time()
        self.quickSort(numeros, 0, len(arreglo2)-1)
        fin = time.time()
        print(f"Tiempo: {fin-inicio}")
    
    def quickSort(self, numeros, izq, der):
        pivote = numeros[izq]
        i = izq
        j = der
        aux = 0

        while i<j:
            while numeros[i]<=pivote and i<j:
                i+=1
            while numeros[j]>pivote:
                j-=1
            if i<j:
                aux = numeros[i]
                numeros[i] = numeros[j]
                numeros[j] = aux

        numeros[izq] = numeros[j]
        numeros[j] = pivote
        if izq<j-1:
            self.quickSort(numeros, izq, j-1)
        if j+1<der:
            self.quickSort(numeros, j+1, der)
    
    def countingSort(self, arr, exp1):
        n = len(arr)
        output = [0] * (n)
        count = [0] * (10)

        for i in range(0, n):
            index = (arr[i]/exp1)
            count[int((index)%10)] += 1
        for i in range(1,10):
            count[i] += count[i-1]
        i = n-1
        while i>=0:
            index = (arr[i]/exp1)
            output[ count[ int((index)%10) ] - 1] = arr[i]
            count[int((index)%10)] -= 1
            i -= 1

        i = 0
        for i in range(0,len(arr)):
            arr[i] = output[i]

    # Method to do Radix Sort
    def radixSort(self, arr):
        max1 = max(arr)
        exp = 1
        inicio = time.time()
        while max1/exp > 0:
            self.countingSort(arr,exp)
            exp *= 10
        fin = time.time()
        print(f"Tiempo: {fin-inicio}")

    def intercalacion(self, a1:list, a2:list) -> list:
        
        a3 : list = []
        cont : int = 0
        cont2 : int = 0

        while len(a1)!=cont and len(a2)!=cont2:

            if a1[cont]<=a2[cont2]:
                a3.append(a1[cont])
                cont+=1
            else:
                a3.append(a2[cont2])
                cont2+=1

        while len(a1)!=cont:
            a3.append(a1[cont])
            cont+=1

        while len(a2)!=cont2:
            a3.append(a2[cont2])
            cont2+=1

        return a3

    def ordenamientoMezclaDirecta(self,array):
        
        mitad=len(array)//2
        
        if len(array)>=2:
            arregloIz=array[mitad:]
            arregloDer=array[:mitad]

            array.clear()  
            
            self.ordenamientoMezclaDirecta(arregloIz)
            
            self.ordenamientoMezclaDirecta(arregloDer)
            
            #Hasta aqui es la divicion de todos los elemntos hasta que llege a ser igual a 1
            while(len(arregloDer)>0 and len(arregloIz)>0):
                if(arregloIz[0]< arregloDer[0]):# si la pocicion de la izquierda es menor a la derecha
                    array.append(arregloIz.pop(0))
                else:
                    array.append(arregloDer.pop(0))
            #Hace que siempre se este actualizando ya que se elimina la pocicion
            
            #Ahora esto es por si llegan a quedar elementos sobrantes
            while len(arregloIz)>0:
                array.append(arregloIz.pop(0))
            
            while len(arregloDer)>0:
                array.append(arregloDer.pop(0))
        
        return array
    
    def mezclaDirecta2(self, array):
        mitad=len(array)//2
        
        if len(array)>=2:
            arregloIz=array[mitad:]
            arregloDer=array[:mitad]

            array.clear()  
            
            self.mezclaDirecta2(arregloIz)
            
            self.mezclaDirecta2(arregloDer)
            
           
            while(len(arregloDer)>0 and len(arregloIz)>0):
                if(arregloIz[0]< arregloDer[0]):
                    array.append(arregloIz.pop(0))
                else:
                    array.append(arregloDer.pop(0))
           
            while len(arregloIz)>0:
                array.append(arregloIz.pop(0))
            
            while len(arregloDer)>0:
                array.append(arregloDer.pop(0))

    def mezclaNatural(self, numeros):
        izquerdo = 0
        izq = 0
        derecho = len(numeros)-1
        der = derecho
        ordenado=False
        
        while(not ordenado):
            ordenado = True
            izquierdo =0
            while(izquierdo<derecho):
                izq=izquerdo
                while(izq < derecho and numeros[izq]<=numeros[izq+1]):
                    izq=izq+1
                der=izq+1
                while(der==derecho-1 or der<derecho and numeros[der]<=numeros[der+1]):
                    der=der+1
                if(der<=derecho):
                    self.mezclaDirecta2(numeros)
                    ordenado= False
                izquierdo = izq
    
    def mostrar(self, arreglo):
        print("Arreglo ordenado:", arreglo)


def generar(cantidad, limite):
    
    array = []
    
    for i in range(0, cantidad):
        array.append(random.randint(0, limite))

    return array


def ingresar():
    while True:
        try:
            cantidad = int(input("Ingresa cantidad de datos: "))
            limite = int(input("Ingresa limite: "))

            return generar(cantidad, limite)
        except ValueError as error:
            print("Error en la entrada de datos <",error,">, prueba de nuevo")
        print()

#--------pruebas--------

mob = MetodosOrdenamiento()

arreglo = ingresar()
arreglo2 = arreglo.copy()

while True:

    print("------------MENU--------------")
    print("1.- Crear nuevo array")
    print("2.- Ordenar por metodo burbuja 1")
    print("3.- Ordenar por metodo burbuja 2")
    print("4.- Ordenar por metodo burbuja 3")
    print("5.- Metodo de insercion")
    print("6.- Metodo de seleccion")
    print("7.- Metodo quick")
    print("8.- Metodo shell")
    print("9.- Metodo radix")
    print("10.- Intercalacion")
    print("11.- Mezcla directa")
    print("12.- Mezcla natural")
    print("13.- Salir")
    
    try:
        opcion = int(input("Ingresa opcion: "))
        
        print(f"Array original: {arreglo2}")
    
        if opcion==1:
            arreglo = ingresar()
            arreglo2 = arreglo.copy()

        elif opcion==2:
            mob.burbuja1(arreglo2)
            mob.mostrar(arreglo2)
    
        elif opcion==3:
            mob.burbuja2(arreglo2)
            mob.mostrar(arreglo2)
    
        elif opcion==4:
            mob.burbuja3(arreglo2)
            mob.mostrar(arreglo2)

        elif opcion==5:
            mob.insercion(arreglo2)
            mob.mostrar(arreglo2)

        elif opcion==6:
            mob.ordenamientoSeleccion(arreglo2)
            mob.mostrar(arreglo2)
        
        elif opcion==7:
            mob.ejecutar(arreglo2)
            mob.mostrar(arreglo2)
        
        elif opcion==8:
            mob.shellSort(arreglo2)
            mob.mostrar(arreglo2)
    
        elif opcion==9:
            mob.radixSort(arreglo2)
            mob.mostrar(arreglo2)
    
        elif opcion==10:
            
            print("Tienes que crea otro arreglo")
            arreglo3 = ingresar()
            mob.quickSort(arreglo2, 0, len(arreglo2)-1)
            mob.quickSort(arreglo3, 0, len(arreglo3)-1)
            
            print("Arreglo 1: ", arreglo2)
            print("Arreglo 2: ", arreglo3)
            
            mob.mostrar(mob.intercalacion(arreglo2, arreglo3))

        elif opcion==11:
            mob.ordenamientoMezclaDirecta(arreglo2)
            mob.mostrar(arreglo2)

        elif opcion==12:
            mob.mezclaNatural(arreglo2)
            mob.mostrar(arreglo2)

        elif opcion==13:
            print("---------FIN---------")
            break
    
        else:
            print("No existe esa opcion")
        print()
    
    except ValueError as error:
        print("Datos invalidos <",error,">, por favor prueba de nuevo\n")
