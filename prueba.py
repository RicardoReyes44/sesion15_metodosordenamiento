'''
Created on 23 nov. 2020

@author: RSSpe
'''


class MetodoOrdenamiento:
    
    
    def ordenamientoMezclaDirecta(self,array):
        
        mitad=len(array)//2
        
        if len(array)>=2:
            arregloIz=array[mitad:]
            arregloDer=array[:mitad]

            array.clear()  
            
            self.ordenamientoMezclaDirecta(arregloIz)
            
            self.ordenamientoMezclaDirecta(arregloDer)
            
            #Hasta aqui es la divicion de todos los elemntos hasta que llege a ser igual a 1
            while(len(arregloDer)>0 and len(arregloIz)>0):
                if(arregloIz[0]< arregloDer[0]):# si la pocicion de la izquierda es menor a la derecha
                    array.append(arregloIz.pop(0))
                else:
                    array.append(arregloDer.pop(0))
            #Hace que siempre se este actualizando ya que se elimina la pocicion
            
            #Ahora esto es por si llegan a quedar elementos sobrantes
            while len(arregloIz)>0:
                array.append(arregloIz.pop(0))
            
            while len(arregloDer)>0:
                array.append(arregloDer.pop(0))
        
        return array


o1=MetodoOrdenamiento()
vector1=[randint(-50,50) for i in range(20)]
print(f"Arreglo sin ordenar: {vector1}")
vector2=o1.ordenamientoMezclaDirecta(vector1)
print(f"Arreglo ordenado: {vector2}")
